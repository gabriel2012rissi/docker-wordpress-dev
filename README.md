# docker-wordpress-dev
![Imagem capa](https://programadoresbrasil.com.br/wp-content/uploads/2020/11/docker-wordpress.png)

Um workflow simples para desenvolvimento utilizando um LEMP com Wordpress. Os créditos pelo desenvolvimento desse projeto são de [aschmelyun/docker-compose-wordpress](https://github.com/aschmelyun/docker-compose-wordpress), então, confira o projeto original e dê uma estrelinha, não custa nada. 

## Utilização

Para começar a utilizar esse projeto é necessário que tenha o [docker](https://docs.docker.com/get-docker/) e o [docker-compose](https://docs.docker.com/compose/install/) devidamente instalados em sua máquina, e então é só clonar esse repositório.

Para obter uma instalação limpa do Wordpress, siga os passos que se encontram em [wordpress/README.md](). Para gerar os certificados de segurança HTTPS, siga os passos descritos em [docker/nginx/ssl/README.md](). Após isso, abra um terminal na pasta do projeto clonado e digite o comando `docker-compose up -d --build` para construir o projeto.

Esse projeto conta com o [Composer](https://getcomposer.org/) para facilitar o gerenciamento de plugins e temas, para utilizá-lo através do docker, dentro do diretório src, use o comando `docker run --rm --interactive --tty --volume $PWD:/app composer install`.

** P.S. Não se esqueça de criar um arquivo .env para suas variáveis de ambiente.** :wink:

### Usando o WP-CLI para gerenciar o Wordpress

Para facilitar a sua vida, utilize no terminal o comando `alias wp="docker-compose run wordpress-cli"`.

Se quiser criar um usuário pelo wp-cli, você poderá se utilizar do comando `wp core install --url="http://localhost" --title="<nome_do_seu_site>" --admin_user="<nome_de_usuário>" --admin_password="<senha_do_usuário>" --admin_email="<email_do_usuário>" --skip-email` ou poderá fazer pelo "método padrão" em [http://localhost](http://localhost).

Caso queira gerenciar os plugins instalados, use o comendo `wp plugin list` e então ative os plugins com, por exemplo, `wp plugin activate akismet` ou `wp plugin activate --all` para ativar a todos os plugins.

Consulte mais comandos no site do [WP-CLI](https://developer.wordpress.org/cli/commands/).

