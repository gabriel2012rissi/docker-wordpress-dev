FROM wordpress:php8.0-fpm-alpine

ENV DIR=/var/www/html

RUN touch /var/log/error_log

ADD ./wordpress/php/fpm/pool.d/www.conf /usr/local/etc/php-fpm.d/www.conf
ADD ./wordpress/php/php.ini-development /usr/local/etc/php/conf.d/conf.ini

WORKDIR ${DIR}
