## Adicione seus certificados aqui para habilitar o https

Uma boa dica é usar o [mkcert](https://github.com/FiloSottile/mkcert) para isso. :wink:

Siga as intruções de instalação, então `cd` para este diretório e use o `mkcert` com o domínio
local de sua escolha.