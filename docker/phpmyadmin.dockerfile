FROM phpmyadmin/phpmyadmin

RUN a2enmod ssl

# Copiando os certificados (inicialmente gerados para o NGINX :P)
ADD ./nginx/ssl/localhost.pem /etc/ssl/certs/self-signed/localhost.pem
ADD ./nginx/ssl/localhost-key.pem /etc/ssl/certs/self-signed/private/localhost-key.pem

ADD ./phpmyadmin/000-default.conf /etc/apache2/sites-available/000-default.conf

EXPOSE 443
