FROM nginx:stable-alpine

ADD ./nginx/ssl /etc/nginx/certs/self-signed
ADD ./nginx/nginx.conf /etc/nginx/conf.d/default.conf
