## Aqui é onde a instalação do Wordpress deve ficar

Para começar, siga os passos abaixo e, ao final, não se esqueça de deletar esse README:

1. Clone seu projeto (caso já exista) ou simplesmente extraia o wordpress para dentro dessa
pasta.
2. Adeque as permissões com os comandos abaixo:

```
sudo chmod -R 777 wp-content
sudo chown -R www-data:www-data .
```